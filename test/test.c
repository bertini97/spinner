#include <stdio.h>
#include <math.h>
#include "spinner.h"

float ferr() {
    return 1.0;
}

int main() {
    size_t side = 50, n_dims=2, n_temps=30, n_probes = 1000+1;
    float temp, temp_start = 0.1, temp_finish=3.0, temp_step, beta;
    spnr_graph_t *graph = spnr_graph_alloc(cubic, ferr, side, n_dims);
    spnr_sys_t *sys = spnr_sys_alloc(graph, nvector, 2);
    spnr_step_t *step = spnr_step_alloc(metropolis, sys);
    spnr_data_t *data = spnr_data_alloc(n_probes);
    size_t i;
    float h_mean, m_mean, h_var, m_var;
    FILE *f;

    temp_step = (temp_finish - temp_start) / n_temps;
    f = fopen("/home/lorenzo/results.txt", "w");
    for (i = 0; i < n_temps; ++i) {
        temp = temp_start + i * temp_step;
        beta = 1.0 / temp;
        spnr_data_run_and_probe(data, sys, step, temp, 10);
        spnr_data_mean_calc(data, &h_mean, &m_mean);
        spnr_data_var_calc(data, &h_var, &m_var);
        printf("temp: %f\n", temp);
        fprintf(f, "%f %+f %+f %+f %+f\n", temp, h_mean, m_mean,
                beta * beta * graph->n_nodes * h_var, beta * graph->n_nodes * m_var);
    }
    fclose(f);

    spnr_data_free(data);
    spnr_step_free(step);
    spnr_sys_free(sys);
    spnr_graph_free(graph);

    return 0;
}
