/* spinner.h
 *
 * Copyright 2024 Lorenzo Bertini
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef SPINNER_H
#define SPINNER_H

/* struct forward declaration */

typedef struct sys_st spnr_sys_t;
typedef struct graph_st spnr_graph_t;
typedef struct step_st spnr_step_t;


/* factor object */

typedef struct {
    size_t n_inds;
    size_t *inds;
    float J;
} factor_t;

/* node object */

typedef struct {
    size_t n_factors;
    factor_t **factor_ptrs;
} node_t;

/* Graph object */

struct graph_st {
    size_t n_factors;
    factor_t *factors;
    size_t *inds_array;

    size_t n_nodes;
    node_t *nodes;
    factor_t **factor_ptrs_array;
};

typedef void (graph_alloc_func)(spnr_graph_t *graph,
                                size_t param1, size_t param2);
void cubic(spnr_graph_t *graph, size_t side, size_t n_dims);

spnr_graph_t *spnr_graph_alloc(graph_alloc_func *alloc_func, float (*getter)(),
                               size_t param1, size_t param2);
void spnr_graph_free(spnr_graph_t *graph);

/* spin kind template */

typedef struct {
    char const *name;
    size_t (*spin_size_in_bytes)(void *priv_ptr);
    void *(*priv_alloc)(size_t N, size_t param);
    void (*priv_free)(void *priv_ptr);
    void (*fill_prop_opp)(void *priv_ptr, void *prop_ptr, size_t k);
    void (*replace)(void *priv_ptr, void *prop_ptr, size_t k);
    float (*factor_H)(void *priv_ptr, size_t n_inds, size_t *inds);
    float (*factor_H_sub)(void *priv_ptr, size_t n_inds, size_t *inds,
                          size_t k, void *spin_ptr);
    float (*calc_m)(void *priv_ptr, size_t n_spins);
} spnr_spin_kind_t;

/* available spin kinds */

extern spnr_spin_kind_t const *ising;
extern spnr_spin_kind_t const *nvector;

/* system object */

struct sys_st {
    spnr_spin_kind_t const *kind;
    void *priv;
    size_t n_spins;
    spnr_graph_t *graph;
};

spnr_sys_t *spnr_sys_alloc(spnr_graph_t *graph, spnr_spin_kind_t const *kind,
                           size_t param);
void spnr_sys_free(spnr_sys_t *sys);
float spnr_sys_calc_h(spnr_sys_t *sys);
float spnr_sys_calc_m(spnr_sys_t *sys);

/* step kind template */

typedef struct {
    char const *name;
    void *(*priv_alloc)(spnr_sys_t *sys);
    void (*priv_free)(void *priv_ptr);
    void (*apply)(void *priv_ptr,spnr_sys_t *sys, float beta);
} spnr_step_kind_t;

/* available step kinds */

extern spnr_step_kind_t const *metropolis;

/* step object */

struct step_st {
    spnr_step_kind_t const *kind;
    void *priv;
};

spnr_step_t *spnr_step_alloc(spnr_step_kind_t const *kind, spnr_sys_t *sys);
void spnr_step_free(spnr_step_t *step);
void spnr_step_apply(spnr_step_t *step, spnr_sys_t *sys, float beta);

/* optional data object */

typedef struct {
    size_t size;
    float *h;
    float *m;
} spnr_data_t;

spnr_data_t *spnr_data_alloc(size_t size);
void spnr_data_free(spnr_data_t *data);
void spnr_data_write(spnr_data_t* data, char const *fname);
void spnr_data_mean_calc(spnr_data_t *data, float *h_mean, float *m_mean);
void spnr_data_var_calc(spnr_data_t *data, float *h_var, float *m_var);
void spnr_data_run_and_probe(spnr_data_t *data, spnr_sys_t *sys, spnr_step_t *step,
                             float temp, size_t n_steps_before_probe);

#endif
