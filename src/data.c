/* data.c
 *
 * Copyright 2024 Lorenzo Bertini
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdio.h>
#include <stdlib.h>
#include "spinner.h"

spnr_data_t *spnr_data_alloc(size_t size) {
    spnr_data_t *data = malloc(sizeof(spnr_data_t));
    data->size = size;
    data->h = malloc(size * sizeof(float));
    data->m = malloc(size * sizeof(float));
    return data;
}

void spnr_data_free(spnr_data_t *data) {
    free(data->h);
    free(data->m);
    free(data);
}

void spnr_data_write(spnr_data_t* data, char const *fname) {
    FILE *f;
    size_t i;

    f = fopen(fname, "w");
    if (!f)
        exit(1);

    for (i = 0; i < data->size; ++i)
        fprintf(f, "%05ld %+.3f %+.3f\n", i, data->h[i], data->m[i]);

    fclose(f);
}

void spnr_data_mean_calc(spnr_data_t *data, float *h_mean, float *m_mean) {
    size_t i, size = data->size;
    float sum_h = 0, sum_m = 0;

    for (i = 0; i < size; ++i) {
        sum_h += data->h[i];
        sum_m += data->m[i];
    }

    *h_mean = sum_h / size;
    *m_mean = sum_m / size;
}

void spnr_data_var_calc(spnr_data_t *data, float *h_var, float *m_var) {
    size_t i, size = data->size;
    float h, sum_h = 0, sum_m = 0, m, sum_sq_h = 0, sum_sq_m = 0;

    for (i = 0; i < size; ++i) {
        h = data->h[i];
        m = data->m[i];
        sum_sq_h += h * h;
        sum_sq_m += m * m;
        sum_h += h;
        sum_m += m;
    }

    *h_var = sum_sq_h / size - (sum_h * sum_h) / (size * size);
    *m_var = sum_m / size - (sum_m * sum_m) / (size * size);
}

void spnr_data_run_and_probe(spnr_data_t *data, spnr_sys_t *sys, spnr_step_t *step,
                        float temp, size_t n_steps_before_probe)
{
    size_t i, j;
    size_t n_probes = data->size;
    float beta = 1.0 / temp;

    data->h[0] = spnr_sys_calc_h(sys);
    data->m[0] = spnr_sys_calc_m(sys);
    for (i = 1; i < n_probes; ++i) {
        for (j = 0; j < n_steps_before_probe; ++j)
            spnr_step_apply(step, sys, beta);
        data->h[i] = spnr_sys_calc_h(sys);
        data->m[i] = spnr_sys_calc_m(sys);
    }
}
