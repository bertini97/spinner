/* graph.c
 *
 * Copyright 2024 Lorenzo Bertini
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdlib.h>
#include <math.h>
#include "spinner.h"

#define N_DIMS_MAX 10

void cubic(spnr_graph_t *gr, size_t side, size_t n_dims) {
    size_t i, j, k;
    size_t unit, row, last_row, left;
    size_t slices[N_DIMS_MAX];
    size_t n_factors_per_node = 2 * n_dims, n_nodes_per_factor = 2;

    for (i = 0; i <= n_dims; ++i)
        slices[i] = pow(side, i);

    gr->n_nodes = slices[n_dims];
    gr->nodes = malloc(gr->n_nodes * sizeof(node_t));
    gr->factor_ptrs_array = malloc(gr->n_nodes * n_factors_per_node * sizeof(factor_t*));

    gr->n_factors = gr->n_nodes * n_dims;
    gr->factors = malloc(gr->n_factors * sizeof(factor_t));
    gr->inds_array = malloc(gr->n_factors * n_nodes_per_factor * sizeof(size_t));

    /*printf("n_nodes=%ld dims=%ld side=%ld\n", g->n_nodes, n_dims, side);*/

    for (i = 0; i < gr->n_factors; ++i) {
        gr->factors[i].n_inds = n_nodes_per_factor;
        gr->factors[i].inds = gr->inds_array + i * n_nodes_per_factor;
    }

    for (i = 0; i < gr->n_nodes; ++i) {
        gr->nodes[i].n_factors = n_factors_per_node;
        gr->nodes[i].factor_ptrs = gr->factor_ptrs_array + i * n_factors_per_node;
    }

    for (i = 0; i < gr->n_nodes; ++i) {
        for (j = 0; j < n_dims; ++j) {
            k = i * n_dims + j;

            unit = slices[j];
            row  = slices[j+1];
            last_row = row - unit;
            left = ((i % row) < unit) ? (i + last_row) : (i - unit);

            gr->factors[k].inds[0] = i;
            gr->factors[k].inds[1] = left;
            gr->nodes[i].factor_ptrs[j] = gr->factors + k;
            gr->nodes[left].factor_ptrs[n_dims + j] = gr->factors + k;
        }
    }
}

spnr_graph_t *spnr_graph_alloc(graph_alloc_func *alloc_func, float (*getter)(),
                     size_t param1, size_t param2) {
    size_t i;
    spnr_graph_t *graph = malloc(sizeof(spnr_graph_t));
    alloc_func(graph, param1, param2);
    for (i = 0; i < graph->n_factors; ++i)
        graph->factors[i].J = getter();
    return graph;
}

void spnr_graph_free(spnr_graph_t *graph) {
    free(graph->factors);
    free(graph->inds_array);
    free(graph);
}

float graph_calc_h(spnr_graph_t *graph, spnr_sys_t *sys) {
    size_t i;
    float H = 0;
    factor_t *fac;
    for (i = 0; i < graph->n_factors; ++i) {
        fac = graph->factors + i;
        H -= fac->J * sys->kind->factor_H(sys->priv, fac->n_inds, fac->inds);
    }
    return H / (float)graph->n_nodes;
}

float graph_calc_H_part(spnr_graph_t *graph, spnr_sys_t *sys, size_t k) {
    size_t i;
    float H_part = 0;
    node_t *node = graph->nodes + k;
    factor_t *fac;
    for (i = 0; i < node->n_factors; ++i) {
        fac = node->factor_ptrs[i];
        H_part -= fac->J * sys->kind->factor_H(sys->priv, fac->n_inds, fac->inds);
    }
    return H_part;
}

float graph_calc_H_part_sub(spnr_graph_t *graph, spnr_sys_t *sys, size_t k, void *spin_ptr) {
    size_t i;
    float H_part = 0;
    node_t *node = graph->nodes + k;
    factor_t *fac;
    for (i = 0; i < node->n_factors; ++i) {
        fac = node->factor_ptrs[i];
        H_part -= fac->J * sys->kind->factor_H_sub(sys->priv, fac->n_inds,
                                                   fac->inds, k, spin_ptr);
    }
    return H_part;
}
