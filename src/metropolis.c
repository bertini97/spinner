/* metropolis.c
 *
 * Copyright 2024 Lorenzo Bertini
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdlib.h>
#include <math.h>
#include "spinner.h"
#include "graph.h"

static void *priv_alloc(spnr_sys_t *sys) {
    return malloc(sys->kind->spin_size_in_bytes(sys->priv));
}

static void priv_free(void *priv_ptr) {
    free(priv_ptr);
}

static int metr_prop_accept(float H_delta, float beta) {
    float acc_ratio, rand_num;
    if (H_delta <= 0)
        return 1;
    else {
        acc_ratio = exp(-beta * H_delta);
        rand_num = rand() / (float)RAND_MAX;
        return rand_num < acc_ratio ? 1 : 0;
    }
}

void apply (void *priv_ptr, spnr_sys_t *sys, float beta) {
    size_t i, k;
    spnr_graph_t *graph = sys->graph;
    size_t n_nodes = graph->n_nodes;
    float H_delta;

    for (i = 0; i < n_nodes; ++i) {
        k = rand() % n_nodes;
        sys->kind->fill_prop_opp(sys->priv, priv_ptr, k);
        H_delta = graph_calc_H_part_sub(graph, sys, k, priv_ptr) -
            graph_calc_H_part(graph, sys, k);
        if (metr_prop_accept(H_delta, beta)) {
            sys->kind->replace(sys->priv, priv_ptr, k);
        }
    }
}

static const spnr_step_kind_t metropolis_kind = {
    "metropolis",
    &priv_alloc,
    &priv_free,
    &apply
};

const spnr_step_kind_t *metropolis = &metropolis_kind;
