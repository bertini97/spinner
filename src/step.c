/* step.c
 *
 * Copyright 2024 Lorenzo Bertini
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdlib.h>
#include <time.h>
#include "spinner.h"

spnr_step_t *spnr_step_alloc (spnr_step_kind_t const *kind, spnr_sys_t *sys)
{
    spnr_step_t * step = malloc(sizeof(spnr_step_t));
    step->kind = kind;
    step->priv = kind->priv_alloc(sys);
    srand(time(0));
    return step;
}

void spnr_step_free (spnr_step_t *step)
{
    step->kind->priv_free (step->priv);
    free (step);
}

void spnr_step_apply (spnr_step_t *step, spnr_sys_t *sys, float beta) {
    step->kind->apply(step->priv, sys, beta);
}

