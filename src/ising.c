/* ising.c
 *
 * Copyright 2024 Lorenzo Bertini
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdlib.h>
#include "spinner.h"

typedef char spin_t;

static size_t spin_size_in_bytes(void *_) {
    return sizeof(spin_t);
}

static void set_up(void *priv_ptr, size_t n_spins) {
    spin_t *spins = (spin_t*)priv_ptr;
    size_t i;

    for (i = 0; i < n_spins; ++i)
        spins[i] = +1.0;
}

static void *priv_alloc(size_t n_spins, size_t _) {
    spin_t *spins = malloc(n_spins * sizeof(spin_t));
    set_up(spins, n_spins);
    return spins;
}

static void priv_free(void *ptr) {
    free(ptr);
}

static void fill_prop_opp(void *priv_ptr, void *prop_ptr, size_t k) {
    spin_t *spins = (spin_t*)priv_ptr;
    spin_t *prop = (spin_t*)prop_ptr;

    *prop = -spins[k];
}

static void replace(void *priv_ptr, void *prop_ptr, size_t k) {
    spin_t *spins = (spin_t*)priv_ptr;
    spin_t *prop = (spin_t*)prop_ptr;

    spins[k] = *prop;
}

static float factor_H(void *priv_ptr, size_t n_inds, size_t *inds) {
    spin_t *spins = (spin_t*)priv_ptr;
    size_t i;
    float mul = 1.0;

    for (i = 0; i < n_inds; ++i)
        mul *= spins[inds[i]];

    return mul;
}

static float factor_H_sub(void *priv_ptr, size_t n_inds, size_t *inds,
                          size_t k, void *spin_ptr) {
    spin_t *spins = (spin_t*)priv_ptr;
    spin_t *spin = (spin_t*)spin_ptr;
    size_t i;
    float mul = 1.0;

    for (i = 0; i < n_inds; ++i)
        mul *= inds[i] != k ? spins[inds[i]] : *spin;

    return mul;
}

static float calc_m(void *priv_ptr, size_t n_spins) {
    spin_t *spins = (spin_t*)priv_ptr;
    size_t i;
    float m = 0;

    for (i = 0; i < n_spins; ++i)
        m += spins[i];

    return m / n_spins;
}

static const spnr_spin_kind_t ising_kind = {
    "ising",
    spin_size_in_bytes,
    priv_alloc,
    priv_free,
    &fill_prop_opp,
    &replace,
    &factor_H,
    &factor_H_sub,
    &calc_m
};

const spnr_spin_kind_t *ising = &ising_kind;
