/* nvector.c
 *
 * Copyright 2024 Lorenzo Bertini
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "spinner.h"

#define N_COMPS_MAX  8
#define PI           3.1415926536

typedef float spin_t;

typedef struct {
    size_t n_comps;
    spin_t *spins;
} nvector_priv_t;

static size_t spin_size_in_bytes(void *priv_ptr) {
    nvector_priv_t *priv = (nvector_priv_t*)priv_ptr;
    return priv->n_comps * sizeof(spin_t);
}

static float rand_gauss () {
    static int has_prev = 0;
    static float prev_rand;
    float u, v, x, y;

    if (has_prev) {
        has_prev = 0;
        return prev_rand;
    } else {
        u = rand() / (float)RAND_MAX;
        v = rand() / (float)RAND_MAX;
        x = sqrt (-2. * log(u)) * sin(2 * PI * v);
        y = sqrt (-2. * log(u)) * cos(2 * PI * v);

        has_prev = 1;
        prev_rand = x;
        return y;
    }
}


static void set_up(void *priv_ptr, size_t n_spins) {
    nvector_priv_t *priv = (nvector_priv_t *)priv_ptr;
    size_t i, j, n_comps = priv->n_comps;

    for (i = 0; i < n_spins; ++i) {
        priv->spins[i * n_comps] = +1.0;
        for (j = 1; j < n_comps; ++j)
            priv->spins[i * n_comps + j] = 0.0;
    }

}

static void *priv_alloc(size_t n_spins, size_t n_comps) {
    nvector_priv_t *priv = malloc(sizeof(nvector_priv_t));
    priv->n_comps = n_comps;
    priv->spins = malloc(n_spins * n_comps * sizeof(spin_t));
    set_up(priv, n_spins);
    return priv;
}

static void priv_free(void *priv_ptr) {
    nvector_priv_t *priv = (nvector_priv_t *)priv_ptr;
    free(priv->spins);
    free(priv);
}

static void spin_rand(spin_t *u, size_t n_comps) {
    size_t i;
    float rand_g, norm = 0;

    for (i = 0; i < n_comps; ++i) {
        rand_g = rand_gauss ();
        u[i] = rand_g;
        norm += rand_g * rand_g;
    }
    norm = sqrt(norm);

    for (i = 0; i < n_comps; ++i)
        u[i] /= norm;
}

static void fill_prop_opp(void *priv_ptr, void *prop_ptr, size_t _) {
    nvector_priv_t *priv = (nvector_priv_t *)priv_ptr;
    spin_t *prop = (spin_t*)prop_ptr;
    spin_rand(prop, priv->n_comps);
}

static void replace(void *priv_ptr, void *prop_ptr, size_t k) {
    nvector_priv_t *priv = (nvector_priv_t *)priv_ptr;
    spin_t *prop = (spin_t*)prop_ptr;
    memcpy(priv->spins + k * priv->n_comps, prop, priv->n_comps * sizeof(spin_t));
}

static float factor_H(void *priv_ptr, size_t n_inds, size_t *inds) {
    nvector_priv_t *priv = (nvector_priv_t *)priv_ptr;
    size_t i, j;
    float mul, sum = 0;
    for (i = 0; i < priv->n_comps; ++i) {
        mul = 1.0;
        for (j = 0; j < n_inds; ++j)
            mul *= priv->spins[inds[j] * priv->n_comps + i];
        sum += mul;
    }
    return sum;
}

static float factor_H_sub(void *priv_ptr, size_t n_inds, size_t *inds,
                          size_t k, void *spin_ptr) {
    nvector_priv_t *priv = (nvector_priv_t *)priv_ptr;
    spin_t *spin = (spin_t*)spin_ptr;
    size_t i, j;
    float mul, sum = 0;

    for (i = 0; i < priv->n_comps; ++i) {
        mul = 1.0;
        for (j = 0; j < n_inds; ++j)
            mul *= inds[j] != k ? priv->spins[inds[j] * priv->n_comps + i] : spin[i];
        sum += mul;
    }

    return sum;
}

static float calc_m(void *priv_ptr, size_t n_spins) {
    nvector_priv_t *priv = (nvector_priv_t *)priv_ptr;
    size_t i, j;
    float m = 0;
    spin_t sum[N_COMPS_MAX];

    memset(sum, 0, sizeof(sum));
    for (i = 0; i < n_spins; ++i)
        for (j = 0; j < priv->n_comps; ++j)
            sum[j] += priv->spins[i * priv->n_comps + j];

    for (j = 0; j < priv->n_comps; ++j) {
        m += sum[j] * sum[j];
    }

    return sqrt(m) / n_spins;
}

static const spnr_spin_kind_t nvector_kind = {
    "ising",
    spin_size_in_bytes,
    priv_alloc,
    priv_free,
    &fill_prop_opp,
    &replace,
    &factor_H,
    &factor_H_sub,
    &calc_m
};

const spnr_spin_kind_t *nvector = &nvector_kind;
