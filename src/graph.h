/* graph.h
 *
 * Copyright 2024 Lorenzo Bertini
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spinner.h"

float graph_calc_h(spnr_graph_t *graph, spnr_sys_t *sys);
float graph_calc_H_part(spnr_graph_t *graph, spnr_sys_t *sys, size_t k);
float graph_calc_H_part_sub(spnr_graph_t *graph, spnr_sys_t *sys, size_t k,
                            void *spin_ptr);
