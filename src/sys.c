/* sys.c
 *
 * Copyright 2024 Lorenzo Bertini
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdlib.h>
#include <string.h>
#include "spinner.h"
#include "graph.h"

spnr_sys_t *spnr_sys_alloc(spnr_graph_t *graph, spnr_spin_kind_t const *kind,
                           size_t param) {
    spnr_sys_t *sys = malloc(sizeof(spnr_sys_t));
    sys->graph = graph;
    sys->n_spins = graph->n_nodes;
    sys->kind = kind;
    sys->priv = kind->priv_alloc(graph->n_nodes, param);
    return sys;
}

void spnr_sys_free(spnr_sys_t *sys) {
    sys->kind->priv_free(sys->priv);
    free(sys);
}

float spnr_sys_calc_h(spnr_sys_t *sys) {
    spnr_graph_t *graph = sys->graph;
    return graph_calc_h(graph, sys);
}

float spnr_sys_calc_m(spnr_sys_t *sys) {
    return sys->kind->calc_m(sys->priv, sys->n_spins);
}
